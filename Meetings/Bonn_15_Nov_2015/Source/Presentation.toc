\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Overview}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Models}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Coupling}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Motivation}{6}{0}{1}
\beamer@sectionintoc {2}{FitzHugh-Nagumo Oscillators}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Patterns of Oscillation}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Single Delay}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Multiple Delays}{16}{0}{2}
\beamer@subsectionintoc {2}{4}{Conclusions}{20}{0}{2}
\beamer@sectionintoc {3}{Biological Model}{21}{0}{3}
\beamer@subsectionintoc {3}{1}{Single Delay}{21}{0}{3}
\beamer@subsectionintoc {3}{2}{Multiple Delays}{24}{0}{3}
\beamer@subsectionintoc {3}{3}{Conclusions}{25}{0}{3}
