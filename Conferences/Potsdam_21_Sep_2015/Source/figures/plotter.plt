set terminal epslatex color colortext standalone solid size 10,5
set output 'Plot.tex'

set xlabel '\Large{\textbf{time in days}}'
set ylabel '\Large{$\mathbf{T_P}$ \textbf{in} $\mathbf{gCm^{-3}}$}'
#set zlabel '\Large{$\mathbf{T_P}$ \textbf{in} $\mathbf{gCm^{-3}}$}'

set title '\LARGE{\textbf{Oscillation Type 2}}'

p 'M_data/Data7.dat' every ::95000::100000 u 1:4 w l lt 3 t ''

set output
!pdflatex Plot.tex
