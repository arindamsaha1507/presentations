set terminal epslatex color colortext standalone solid size 10,5
set output 'Plot.tex'

set xlabel '\Large{\textbf{time in days}}'
set ylabel '\Large{$\mathbf{T_P}$ \textbf{in} $\mathbf{gCm^{-3}}$}'

set title '\LARGE{$\mathbf{\theta=0.01}$}'

p 'Run38/Data1.dat' every ::80000::100000 u 1:4 w l t ''
set output
!pdflatex Plot.tex>>logfile
