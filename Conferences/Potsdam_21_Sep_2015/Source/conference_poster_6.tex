%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a0poster Portrait Poster
% LaTeX Template
% Version 1.0 (22/06/13)
%
% The a0poster class was created by:
% Gerlinde Kettl and Matthias Weiser (tex@kettl.de)
% 
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0,portrait]{a0poster}

\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
\columnseprule=3pt % This is the thickness of the black line between the columns in the poster

\usepackage[svgnames]{xcolor} % Specify colors by their 'svgnames', for a full list of all colors available see here: http://www.latextemplates.com/svgnames-colors

\usepackage{times} % Use the times font
%\usepackage{palatino} % Uncomment to use the Palatino font

\usepackage{graphicx} % Required for including images
\graphicspath{{figures/}} % Location of the graphics files
\usepackage{booktabs} % Top and bottom rules for table
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{amsfonts, amsmath, amsthm, amssymb} % For math fonts, symbols and environments
\usepackage{wrapfig} % Allows wrapping text around tables and figures
\usepackage{subcaption}

\begin{document}

%----------------------------------------------------------------------------------------
%	POSTER HEADER 
%----------------------------------------------------------------------------------------

% The header is divided into two boxes:
% The first is 75% wide and houses the title, subtitle, names, university/organization and contact information
% The second is 25% wide and houses a logo for your university/organization or a photo of you
% The widths of these boxes can be easily edited to accommodate your content as you see fit

\begin{minipage}[b]{0.75\linewidth}
\veryHuge \color{NavyBlue} \textbf{Propagation of Harmful Algal Blooms in Oceans} \color{Black}\\ % Title
\Huge\textit{A Network Approach}\\[2cm] % Subtitle
\huge \textbf{Arindam Saha, Ulrike Feudel}\\[0.5cm] % Author(s)
\huge Theoretical Physics/Complex Systems, ICBM, University of Oldenburg, 26129 Oldenburg, Germany\\[0.4cm] % University/organization
\Large \texttt{arindam.saha@uni-oldenburg.de}\\
\end{minipage}
%
\begin{minipage}[b]{0.25\linewidth}
\centering
\includegraphics[width=0.7\linewidth]{logo.pdf}\\
\end{minipage}

\vspace{0.1cm} % A bit of extra whitespace between the header and poster content

%----------------------------------------------------------------------------------------

\begin{multicols}{2} % This is how many columns your poster will be broken into, a portrait poster is generally split into 2 columns

%----------------------------------------------------------------------------------------
%	MOTIVATION
%----------------------------------------------------------------------------------------

\color{Navy} % Navy color for the abstract

\section*{Motivation}

\begin{minipage}{0.4\linewidth}
\begin{itemize}
\item \textbf{Harmful Algal Blooms} are extreme events which occur in oceans where the population of toxic phytoplankton in certain regions of ocean suddenly increases for a short period of time and has severe effects on the food web.

\item The severity of these blooms and the unpredictability of their times of occurrence make it an important area of investigation. 

\item The occurrence of harmful algal blooms in a location depends on the nutrient and plankton concentration which in turn depends on two factors:
\begin{itemize}
\item Local environmental and biological factors
\item Transport via ocean currents
\end{itemize}
\end{itemize}
\end{minipage}
\hspace{0.05\linewidth}
\begin{minipage}{0.58\linewidth}
\begin{center}\vspace{1cm}
\includegraphics[width=0.3\linewidth, height=0.3\linewidth]{HAB.png}
\includegraphics[width=0.3\linewidth, height=0.3\linewidth]{map.png}
\includegraphics[width=0.3\linewidth, height=0.3\linewidth]{flow.png}
~\\~\\
\includegraphics[width=0.3\linewidth, height=0.3\linewidth]{rates.png}
\includegraphics[width=0.3\linewidth, height=0.3\linewidth]{Connections1.png}
\includegraphics[width=0.3\linewidth, height=0.3\linewidth]{Connections2.png}

\captionof{figure}{\color{Green} The occurrence of a HAB in nature (top left). Studies on the coasts of Southern California (top centre) have shown the existence of complex flow patterns of the ocean currents (top right) [Image Courtesy: Dong et. al.] which lead to various time delays along the coasts (bottom left)) [Image Courtesy: Mitarai et. al.]. These flows form networks through which nutrients and plankton might be carried (bottom centre and bottom right)[Image Courtesy: Bialonski et. al.].}
\end{center}\vspace{1cm}
\end{minipage}

\color{DimGray}

\section*{Objective}

We try to understand the impact of transport of nutrients and planktons on the occurrence of harmful algal blooms by modelling the system as a network of time-delay coupled nodes.


%----------------------------------------------------------------------------------------
%	THE DYNAMICAL SYSTEM
%----------------------------------------------------------------------------------------

\color{SaddleBrown} % SaddleBrown color for the introduction

\section*{The Dynamical System}

\begin{minipage}{0.55\linewidth}
Each node is characterised by four dynamical variables: the nutrient concentration $N$, the non-toxic phytoplankton concentration $N_P$, toxic phytoplankton concentration $T_P$ and zooplankton concentration $Z$. The internal dynamics of each node is given by:
\end{minipage}
\hspace{0.05\linewidth}
\begin{minipage}{0.4\linewidth}
\begin{center}\vspace{1cm}
\includegraphics[width=\linewidth]{nodes.pdf}
\captionof{figure}{\color{Green} Decomposing the network}
\end{center}
\end{minipage}

\begin{align*}
\left(\dfrac{dN}{dt}\right)_{internal} & = k (N_0-N) - f(N) g(N_P,T_P) (N_P+T_P) + r (N_P+T_P) \nonumber \\
~ & + \beta (h_1(N_P,T_P)+h_2(N_P,T_P)) Z + \gamma (d+\theta h(T_P)) Z \\
\left(\dfrac{dN_P}{dt}\right)_{internal} & = \theta_1 f(N) g(N_P,T_P) N_P - r N_P - h_1(N_P,T_P) Z - (s+k) N_P \\
\left(\dfrac{dT_P}{dt}\right)_{internal} & = \theta_2 f(N) g(N_P,T_P) T_P - r T_P - h_2(N_P,T_P) Z - (s+k) T_P \\
\left(\dfrac{dZ}{dt}\right)_{internal} & = (\alpha_1 h_1(N_P,T_P) + \alpha_2 h_2(N_P,T_P)) Z - (d+ \theta h(T_P)) Z
\end{align*}
where $\theta$ is the sensitivity of zooplankton to the toxins.

Hence the complete dynamics of a dynamical variable $x_i$ at node $i$ is given as

\begin{equation*}
\dfrac{dx_i(t)}{dt} = \left(\dfrac{dx_i(t)}{dt}\right)_{internal} - \sum_{j,k} M_{jik} x_i(t) + \sum_{j,k} M_{ijk} x_j(t-\tau_{ijk}) \mathcal{H}(t-\tau_{ijk})
\end{equation*}

where $M_{ijk}$ denotes the rate of flow of nutrients and plankton from node $j$ to node $i$ via layer $k$ and $\tau_{ijk}$ denotes the corresponding time delay.


%----------------------------------------------------------------------------------------
%	Results
%----------------------------------------------------------------------------------------

\color{DarkSlateGray} % DarkSlateGray color for the rest of the content

\section*{Results}

\subsection*{Independent Dynamics}

\begin{center}
\includegraphics[width=0.3\linewidth]{Normal1.pdf}
\includegraphics[width=0.3\linewidth]{Normal2.pdf}
\includegraphics[width=0.3\linewidth]{Normal3.pdf}
\captionof{figure}{\color{Green} Without Seasonal Forcing on Nutrients}
\includegraphics[width=0.3\linewidth]{Normal4.pdf}
\includegraphics[width=0.3\linewidth]{Normal5.pdf}
\includegraphics[width=0.3\linewidth]{Normal6.pdf}
\captionof{figure}{\color{Green} With Seasonal Forcing on Nutrients}
\end{center}

\textbf{Increase in toxin sensitivity of zooplanton reduces the frequencies of harmful algal blooms and makes them chaotic. This effect is enhanced if the seasonal variation of surface water nutrient concentration is taken into consideration.}

\subsection*{Network Dynamics}

For most of our results, we use a network of two nodes with bi-directional coupling.

\begin{enumerate}
\item \textbf{Effect of Changes in Time Delay}
\begin{enumerate}
\item \textbf{Death of Oscillation:} 

%\begin{center}
\begin{minipage}{0.7\linewidth}
\includegraphics[width=\linewidth]{Bif_tau.pdf}
\end{minipage}
\begin{minipage}{0.25\linewidth}
\begin{center}
\includegraphics[width=\linewidth]{tau_osc.pdf}
\includegraphics[width=\linewidth]{tau_bistable.pdf}
\includegraphics[width=\linewidth]{tau_death.pdf}
\end{center}
\end{minipage}
\captionof{figure}{\color{Green} The left part shows the bifurcation plot for $\tau$. As $\tau$ increases, the oscillator system enters a bistable region via a reverse Hopf bifurcation and then an amplitude death occurs in a fold bifurcation of limit cycles. The right part of the figure shows the corresponding time series for the three states.}
~\\
\textbf{Longer delays reduce the intensity of algal blooms and might even eliminate them completely}
~\\
%\end{center}
\end{enumerate}

\item \textbf{Effect of Changes in Coupling Strength}
\begin{enumerate}
\item \textbf{Changes in Oscillatory Patterns: }

\begin{minipage}{0.7\linewidth}
\begin{center}
\includegraphics[width=0.9\linewidth]{M_Phase.pdf}
\end{center}
\end{minipage}
\begin{minipage}{0.25\linewidth}
\begin{center}
\includegraphics[width=\linewidth]{M1.pdf}
\includegraphics[width=\linewidth]{M2.pdf}
\end{center}
\end{minipage}
\captionof{figure}{\color{Green} The left part shows phase space trajectories of the nodes for varying rates of flow between the nodes showing the two distinct sets of limit cycles. The right part shows the corresponding temporal dynamics.}
~\\
\textbf{Algal blooms are easier to suppress in oscillations of Type 1 as the toxic phytoplankton concentration approaches zero periodically and might be eliminated by random fluctuations, and a higher flow rate increases the severity of blooms and make them less prone to fluctuations }
~\\

\item \textbf{Delaying Chaotic Oscillations: } 

\begin{center}
\includegraphics[width=0.35\linewidth]{chaos_supp1.pdf}
\includegraphics[width=0.35\linewidth]{chaos_supp2.pdf}
\captionof{figure}{\color{Green} The temporal dynamics of a single node (left) and the same node when connected to the network (right).}
\end{center}
~\\
\textbf{Presence of ocean currents and delay might turn a chaotic temporal dynamics of algae populations into a regular oscillatory dynamics, hence increasing the frequency of harmful algal blooms}
~\\

\end{enumerate}
\end{enumerate}


\color{SaddleBrown} % SaddleBrown color for the conclusions to make them stand out

%----------------------------------------------------------------------------------------
%	FORTHCOMING RESEARCH
%----------------------------------------------------------------------------------------

\section*{Outlook: Goals for the Future}

\begin{enumerate}
\item Exploring the intricate interplay between harmful algal blooms, flow patterns and delay
\item Studying the phase synchrony between the possible oscillatory states of the system.
\item Investigating the effect of network topology on the system dynamics
\end{enumerate}

\begin{center}
\includegraphics[width=0.3\linewidth]{Suppression.pdf}
\includegraphics[width=0.3\linewidth]{Phase_Shift.pdf}
\includegraphics[width=0.3\linewidth]{Star.pdf}
\captionof{figure}{\color{Green} Glimpses of the future}
\end{center}

\color{DarkSlateGray}

 %----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\nocite{*} % Print all references regardless of whether they were cited in the poster or not
\bibliographystyle{plain} % Plain referencing style
\bibliography{sample} % Use the example bibliography file sample.bib
%----------------------------------------------------------------------------------------

\end{multicols}
\end{document}