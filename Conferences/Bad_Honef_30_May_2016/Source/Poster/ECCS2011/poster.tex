% --------------------------------------------------------------------------- %
% Poster for the ECCS 2011 Conference about Elementary Dynamic Networks.      %
% --------------------------------------------------------------------------- %
% Created with Brian Amberg's LaTeX Poster Template. Please refer for the     %
% attached README.md file for the details how to compile with `pdflatex`.     %
% --------------------------------------------------------------------------- %
% $LastChangedDate:: 2011-09-11 10:57:12 +0200 (V, 11 szept. 2011)          $ %
% $LastChangedRevision:: 128                                                $ %
% $LastChangedBy:: rlegendi                                                 $ %
% $Id:: poster.tex 128 2011-09-11 08:57:12Z rlegendi                        $ %
% --------------------------------------------------------------------------- %
\documentclass[a0paper,portrait]{baposter}

\usepackage{relsize}		% For \smaller
\usepackage{url}			% For \url
\usepackage{epstopdf}	% Included EPS files automatically converted to PDF to include with pdflatex
\usepackage{amsmath}
\usepackage{tcolorbox}
\usepackage{caption}
\usepackage{flafter}

%%% Global Settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\graphicspath{{pix/}}	% Root directory of the pictures 
\tracingstats=2			% Enabled LaTeX logging with conditionals

%%% Color Definitions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\definecolor{bordercol}{RGB}{40,40,40}
\definecolor{headercol1}{RGB}{186,215,230}
\definecolor{headercol2}{RGB}{80,80,80}
\definecolor{headerfontcol}{RGB}{0,0,0}
\definecolor{boxcolor}{RGB}{255,255,255}
%\definecolor{boxcolor}{RGB}{176,224,230}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Utility functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Save space in lists. Use this after the opening of the list %%%%%%%%%%%%%%%%
\newcommand{\compresslist}{
	\setlength{\itemsep}{1pt}
	\setlength{\parskip}{0pt}
	\setlength{\parsep}{0pt}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Document Start %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\typeout{Poster rendering started}

%%% Setting Background Image %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\background{
%	\begin{tikzpicture}[remember picture,overlay]%
%	\draw (current page.north west)+(-2em,2em) node[anchor=north west]
%	{\includegraphics[height=1.1\textheight]{background}};
%	\end{tikzpicture}
}

%%% General Poster Settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Eye Catcher, Title, Authors and University Images %%%%%%%%%%%%%%%%%%%%%%
\begin{poster}{
	grid=false,
	% Option is left on true though the eyecatcher is not used. The reason is
	% that we have a bit nicer looking title and author formatting in the headercol
	% this way
	%eyecatcher=false, 
	borderColor=bordercol,
	headerColorOne=headercol1,
	headerColorTwo=headercol2,
	headerFontColor=headerfontcol,
	% Only simple background color used, no shading, so boxColorTwo isn't necessary
	boxColorOne=boxcolor,
	headershape=roundedright,
	headerfont=\Large\sf\bf,
	textborder=rectangle,
	background=user,
	headerborder=open,
  boxshade=plain
}
%%% Eye Cacther %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
\begin{minipage}{14em}
\begin{center}
\includegraphics[width=0.5\textwidth, height=0.5\textwidth]{figures/ICBM_Logo.png}
\includegraphics[width=0.8\textwidth, height=0.4\textwidth]{figures/Uni_oldenburg_logo.png}
\end{center}
%			\includegraphics[width=10em,height=4em]{colbud_logo}
%			\includegraphics[width=4em,height=4em]{elte_logo} \\
%			\includegraphics[width=10em,height=4em]{dynanets_logo}
%			\includegraphics[width=4em,height=4em]{aitia_logo}
		\end{minipage}{figures/logo.pdf}
%	Eye Catcher, empty if option eyecatcher=false - unused
}
%%% Title %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{\sf\bf~\\~\\~\\~\\
	Extreme Events in Delay-Coupled FitzHugh-Nagumo Oscillators 
}
%%% Authors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
	\vspace{1em} Arindam Saha, Ulrike Feudel\\
	{\smaller Institute for Chemistry and Biology of the Marine Environment \\
Carl von Ossietzky University Oldenburg, Oldenburg, Germany\\arindam.saha@uni-oldenburg.de\\~\\~\\}
~\\~\\~\\
}
%%% Logo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
% The logos are compressed a bit into a simple box to make them smaller on the result
% (Wasn't able to find any bigger of them.)
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.0pt}
	\fbox{
		\begin{minipage}{14em}
			\includegraphics[width=\textwidth, height=0.8\textwidth]{figures/Logo.eps}
%			\includegraphics[width=10em,height=4em]{colbud_logo}
%			\includegraphics[width=4em,height=4em]{elte_logo} \\
%			\includegraphics[width=10em,height=4em]{dynanets_logo}
%			\includegraphics[width=4em,height=4em]{aitia_logo}
		\end{minipage}
	}
}
~\\
~\\
\headerbox{Introduction}{name=problem,column=0,row=0}{

Extreme events are rare, aperiodic, recurrent events which have a large impact on dynamical systems.

\textbf{Known factors inducing Extreme Events}
\begin{itemize}
\setlength\itemsep{0.0em}
\item Noise induced attractor hopping in laser systems
\item Inhomogeneity of parameters in excitable systems
%\item Many more...
\end{itemize}

\textbf{Objective}

To study the role of time-delayed diffusive coupling in the generation of extreme events in excitable relaxation oscillators.

}

\headerbox{The Model}{name=definitions,column=0,below=problem}{
Let us consider a system of two coupled FitzHugh-Nagumo (FHN) units whose dynamics is given as
\begin{small}
\begin{equation*}
\begin{aligned}
\dot{x}_i &= x_i(a-x_i)(x_i-1)-y_i + \sum_{k=1}^L M_k (x_j^{(\tau_k)}-x_i) \\
\dot{y}_i &= b x_i - c y_i + \sum_{k=1}^L M_k (y_j^{(\tau_k)}-y_i). 
\end{aligned}
\label{eq: Model}
\end{equation*}
\end{small}
where $i,j \in \{1,2\}$ and $i \neq j$.
~\\~\\
\begin{minipage}{0.6\textwidth}
\begin{small}
$a,b,c$ : Internal parameters of the units \\
$L$ : Number of layers \\
$M_k$ : Coupling strength of the $k^{th}$ layer \\
$\tau_k$ : Time-delay corresponding to the $k^{th}$ layer \\
\end{small}
\end{minipage}
\begin{minipage}{0.05\textwidth}
~
\end{minipage}
\begin{minipage}{0.3\textwidth}
\begin{flushright}
\includegraphics[width=\textwidth]{figures/Network_Small.eps}
\end{flushright}
\end{minipage}
\captionof{figure}{Coupling the FHN units}
}

\headerbox{Fixed Points}{name=models,column=0,below=definitions}{

Coupling of the two FHN units results in formation of two pairs of fixed points: one stable; one unstable. %This is in addition to the unstable fixed point at the origin which is present even when the units are uncoupled.

\includegraphics[width=\textwidth]{figures/Plot_FP_80.eps}
\captionof{figure}{The $x$- and $y-$ coordinates of the stable and unstable fixed points for varying coupling strengths. Here $M=M_1+M_2$.}
}

%\headerbox{References}{name=references,column=0,below=models}{
%\smaller													% Make the whole text smaller
%\vspace{-0.4em} 										% Save some space at the beginning
%\bibliographystyle{plain}							% Use plain style
%\renewcommand{\section}[2]{\vskip 0.05em}		% Omit "References" title
%\begin{thebibliography}{1}							% Simple bibliography with widest label of 1
%\itemsep=-0.01em										% Save space between the separation
%\setlength{\baselineskip}{0.4em}					% Save space with longer lines
%\bibitem{prevWork1} Laszlo Gulyas, Richard Legendi: \emph{Effects of Sample Duration on Network Statistics in Elementary Models of Dynamic Networks}, International Conference on Computational Science, Singapore (2011) 
%\bibitem{prevWork2} Laszlo Gulyas, Susan Khor, Richard Legendi and George Kampis \emph{Cumulative Properties of Elementary Dynamic Networks}, The International Sunbelt Social Network Conference XXXI (2011)
%\bibitem{gulya-kampis1} Gulyas, Laszlo et al.: \emph{Betweenness Centrality Dynamics in Networks of Changing Density}. Presented at the 19th International Symposium on Mathematical Theory of Networks and Systems (MTNS 2010)
%\end{thebibliography}
%}

\headerbox{References}{name=acknowledgements,column=0,below=models, above=bottom}{
%\begin{small}
\begin{enumerate}
\setlength\itemsep{0.0em}
\item Ansmann, G., Karnatak, R., Lehnertz, K. \& Feudel, U. (2013). Physical Review E, 88(5), 052911.
\item Lai, Y. C., Grebogi, C., Yorke, J. A. \& Venkataramani, S. C. (1996). Physical review letters, 77(1), 55.
\end{enumerate}
%\end{small}
%\smaller						% Make the whole text smaller
%\vspace{-0.4em}			% Save some space at the beginning
%This research was partially supported by the Hungarian Government (KMOP-1.1.2-08/1-2008-0002 ) and the European Union's Seventh Framework Programme: DynaNets, FET-Open project no. FET-233847 (\url{http://www.dynanets.org}). The supports are gratefully acknowledged.
} 

\headerbox{Dynamical Properties}{name=density,span=2,column=1,row=0}{
The diagonal of the phase space --- defined by $x_1=x_2$; $y_1=y_2$ --- is an invariant synchronisation manifold whose stability plays a crucial role in determining the dynamics of the trajectories.% starting away from it.
\begin{center}
\begin{minipage}{0.3\textwidth}
\begin{tcolorbox}[title=One Delay\\ $\tau$ as 80]
\begin{center}
\includegraphics[width=\linewidth]{figures/One_Layer_80.eps}\\
\includegraphics[width=\linewidth]{figures/3D_Final_80.eps}
\end{center}
\end{tcolorbox}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\begin{tcolorbox}[title=One Delay\\ $\tau$ as 70]
\begin{center}
\includegraphics[width=\linewidth]{figures/One_Layer_70_2.eps}\\
\includegraphics[width=\linewidth]{figures/3D_One_Layer_70.eps}
\end{center}
\end{tcolorbox}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\begin{tcolorbox}[title={Two Delays\\ $\tau_1$ as 80 and $\tau_2$ as 65}]
\begin{center}
\includegraphics[width=\linewidth]{figures/Plot_OffDiag_2.eps}\\
\includegraphics[width=\linewidth]{figures/3D_Two_Layer.eps}
\end{center}
\end{tcolorbox}
\end{minipage}
\end{center}
%\captionof{figure}{Dynamics of the FHN units for various types of coupling}
\begin{minipage}{0.55\textwidth}
\textbf{Two types of Events}
\begin{itemize}
\setlength\itemsep{0.0em}
\item Type-1: In-Phase event where trajectory lies on the invariant synchronisation manifold.
\item Type-2: Out-of-Phase event which occurs away from the invariant synchronisation manifold.
\end{itemize}
\textbf{Single delay coupling induces events of either Type-1 (Long delays where invariant manifold is stable) or Type-2 (Short delays where invariant manifold is not stable). Applying both delays together allows for both types of events to occur.}
%\begin{itemize}
%\item For \textbf{single layer} coupling with \textbf{long} delays $(\tau=80)$, the \textbf{invariant manifold is stable}. \textbf{All large amplitude oscillations} occur on the manifold. Hence all events are \textbf{in-phase}.
%\item For \textbf{single layer} coupling with \textbf{short} delays $(\tau=70)$, the \textbf{invariant manifold is not stable}. \textbf{No large amplitude oscillations} occur on the manifold. Hence all events are \textbf{out-of-phase}.
%\item For \textbf{two layer} coupling with \textbf{long and short} delays $(\tau_1=80;\tau_2=65)$, the \textbf{invariant manifold is not stable}. \textbf{Some} large amplitude oscillations occur on the manifold.
%\end{itemize}
\end{minipage}
\begin{minipage}{0.02\textwidth}
~\\
\end{minipage}
\begin{minipage}{0.2\textwidth}
\begin{tcolorbox}[title=In-phase Event]
\centering
\includegraphics[width=\textwidth, height=0.1\textheight]{figures/InPhaseEvent.eps}
\end{tcolorbox}
\end{minipage}
\begin{minipage}{0.2\textwidth}
\begin{tcolorbox}[title=Out-of-phase Event]
\centering
\includegraphics[width=\textwidth, height=0.1\textheight]{figures/OutPhaseEvent.eps}
\end{tcolorbox}
\end{minipage}

\begin{center}
\begin{tcolorbox}[title=Analysing the Dynamics]
\begin{minipage}{0.45\textwidth}
\includegraphics[width=\linewidth, height=0.5\linewidth]{figures/ISI_One_Two_Layer.eps}\\
\textit{(a) Inter-event intervals for one delay ($\tau=80$ in red; $\tau=70$ in blue) and two delays ($\tau_1=80$, $\tau_2=65$ in magenta) for varying total coupling strength.}
\includegraphics[width=\linewidth, height=0.5\linewidth]{figures/LimitCycleBif.eps}
\textit{(c) Bifurcation plot showing period doubling of the transversally unstable limit-cycle which causes riddling-like behaviour between basins of attraction.}
\end{minipage}
\begin{minipage}{0.05\textwidth}
~\\
\end{minipage}
\begin{minipage}{0.45\textwidth}
\includegraphics[width=\linewidth, height=0.5\linewidth]{figures/Plot_Phase.eps}\\
\textit{(b) Loss of phase synchrony much prior to Type-2 events allowing it to be used as a precursor to them.\\~\\}
\includegraphics[width=0.5\textwidth, height=0.5\textwidth]{figures/Magnified_One_M_0_0125.eps}~
\includegraphics[width=0.5\textwidth, height=0.5\textwidth]{figures/Magnigied_Two_M_0_0125.eps}
\textit{(d) Basins of attraction of fixed points when invariant manifold is stable (left) and unstable (right) respectively.\\~ }
\end{minipage}
%\captionof{figure}{Analysing the Dynamics}
\end{tcolorbox}
\end{center}
}

%Long delays ($\tau=80$): The invariant manifold is stable. Hence, any trajectory starting away from the diagonal finally converge to it and perform mixed mode oscillations on the manifold. Short delays ($\tau=70$): The invariant manifold loses its stability. Although trajectories starting away from the diagonal come very close to the diagonal and generate small oscillations, they occasionally diverge away from it making a large amplitude oscillation. Thereafter, they either get converged to the fixed points or come back near the diagonal.}

\headerbox{Conclusions}
{name=degreeDistribution,span=2,column=1,below=density,above=bottom}{
\begin{itemize}
\setlength\itemsep{0.0em}
\item Extreme events may be induced in parametrically identical FHN oscillators using multiple time-delayed diffusive couplings.
\item Extreme events may occur either in- or out-of-phase.
\item Loss of synchrony has been identified as a precursor to the out-of-phase extreme events.
\end{itemize}
%\begin{itemize}
%\item Introduction of the second delay makes the dynamics more chaotic; making the occurrence of large amplitude oscillation less regular and hence giving rise to extreme events.
%
%\item Loss of synchrony occurs prior to Type-2 extreme events and hence can be used as a precursor for them.
%
%%\item When the invariant manifold is stable; the trajectories can converge to the fixed point only if it never comes close to the diagonal. In contrast, when stability of the invariant manifold is lost, the trajectory may come lose to it multiple times before converging to the fixed point. This makes the basins of attraction in the second case larger and more complex.
%\end{itemize}
}

\end{poster}
\end{document}
