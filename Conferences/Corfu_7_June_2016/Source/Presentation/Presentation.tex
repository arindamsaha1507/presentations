\documentclass[11pt]{beamer}
\usetheme{Singapore}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{color}
\author{\textbf{Arindam Saha}, Ulrike Feudel}
\title{Extreme Events in Delay-Coupled FitzHugh-Nagumo Oscillators}
\date{
\includegraphics[scale=0.3]{ICBM_Logo.png}~~
\includegraphics[scale=0.4]{Uni_oldenburg_logo.png}~~
\includegraphics[scale=0.06]{Logo.eps}
~\\~\\
Dynamics Days 2016
}
%\setbeamercovered{transparent} 
\setbeamertemplate{navigation symbols}{} 
%\logo{} 
\institute{Theoretical Physics/Complex Systems \\ Institute for Chemistry and Biology of Marine Environment \\  Carl von Ossietzky Universit\"at, Oldenburg, Germany} 
%\date{} 
%\subject{} 
\begin{document}

\begin{frame}
\titlepage
\end{frame}

%\begin{frame}
%\tableofcontents
%\end{frame}

\begin{frame}{Extreme Events}
\begin{block}{Definition}
Extreme events are rare, aperiodic, recurrent events which have a large impact on dynamical systems.
\end{block}
\begin{minipage}{0.45\textwidth}
\begin{block}{Examples}
\begin{itemize}
\item Harmful Algal Blooms
\item Epileptic Seizures
\item Extreme weather conditions
%\item Social Situations: Opinion Dynamics
\item Financial Crashes
\item Natural Disasters
\end{itemize}
\end{block}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\includegraphics[width=\textwidth]{Toxic_Algae_Bloom_in_Lake_Erie.jpg}
\end{minipage}
\end{frame}

\begin{frame}{Extreme Events}
\begin{block}{Definition}
Extreme events are rare, aperiodic, recurrent events which have a large impact on dynamical systems.
\end{block}
\begin{minipage}{0.45\textwidth}
\begin{block}{Examples}
\begin{itemize}
\item Harmful Algal Blooms
\item Epileptic Seizures
\item Extreme weather conditions
%\item Social Situations: Opinion Dynamics
\item Financial Crashes
\item Natural Disasters
\end{itemize}
\end{block}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{block}{Known Mechanisms}
\begin{itemize}
\item \textbf{Noise} induced attractor hopping in laser systems
\item \textbf{Inhomogeneity of parameters} in networks of excitable systems
\item Chimera states in small-world networks of \textbf{pulse-coupled} oscillators
\end{itemize}
\end{block}
\end{minipage}
\\
\begin{center}
\textbf{Can time-delay cause extreme events?}
\end{center}
\end{frame}

%\begin{frame}{Extreme Events}
%\begin{block}{Definition}
%Extreme events are rare, aperiodic, recurrent events which have a large impact on dynamical systems.
%\end{block}
%\begin{block}{Known Mechanisms}
%\begin{itemize}
%\item Noise induced attractor hopping in laser systems
%\item Inhomogeneity of parameters in excitable systems
%\item Many more...
%\end{itemize}
%\end{block}
%\end{frame}

\begin{frame}
\frametitle{Fitz-Hugh Nagumo Oscillators}
\begin{minipage}{0.39\textwidth}
\begin{block}{Definition}
\begin{align*}
\dot{x} &= x(a-x)(x-1)-y \\
\dot{y} &= bx-cy
\end{align*}
\end{block}
\begin{block}{Characteristics}
\begin{itemize}
%\item Two dimensional relaxation oscillator
\item Dynamical Variables
\begin{itemize}
\item Activator variable \textcolor{blue}{$x$}: with a cubic nullcline
\item Inhibitor variable \textcolor{red}{$y$}: with a linear nullcline
\end{itemize}
\item Parameters: \textcolor{magenta}{$a$, $b$, $c$}
\end{itemize}
\end{block}
\end{minipage}
\begin{minipage}{0.59\textwidth}
\begin{figure}
\includegraphics[width=\textwidth,height=0.8\textheight]{Nullclines.png}
\end{figure}
\end{minipage}
\end{frame}

\begin{frame}{FHN Units: Dynamical Regimes}
\begin{figure}
\includegraphics[width=\textwidth]{TwoTypes.eps}
\end{figure}
\textbf{Parameters chosen: }\\
$a=-0.025, b=0.00652, c=0.02$ \\
For the chosen set of parameters, the independent FHN units are in the \alert{oscillatory regime.}
\end{frame}


\begin{frame}
\frametitle{Objective}
\begin{block}{Previous Research}
\textbf{Instantaneous} diffusive coupling of FHN oscillators can
\begin{itemize}
\item exhibit \textbf{extreme events} \textit{provided} the oscillators are \textbf{non-identical in terms of intrinsic parameters}
\item at best exhibit \textbf{mixed-mode oscillations} if the oscillators are \textbf{identical in terms of intrinsic parameters}
\end{itemize}
\end{block}
\begin{block}{Question Addressed}
Can \textbf{time-delayed diffusive} coupling of FHN oscillators lead to \textbf{extreme events} in oscillators with \textbf{identical intrinsic parameters}?
\end{block}
\end{frame}

\begin{frame}{Invariant Synchronisation Manifold}
%\begin{minipage}{0.49\textwidth}
\begin{itemize}
\item If the two oscillators start with identical initial conditions, their dynamics will be identical for all times.
\item Manifold defined by \alert{$x_1=x_2$; $y_1=y_2$} is invariant.
\item Stability of this manifold is crucial to the dynamics in the entire phase space.
\end{itemize}
%\end{minipage}
%\begin{minipage}{0.49\textwidth}
\begin{figure}
\includegraphics[width=\textwidth]{Invariant_Manifold.eps}
\end{figure}
%\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Coupling the FHN Oscillators}
\begin{minipage}{0.45\textwidth}
\begin{align*}
\mathbf{\dot{X_1}}(t) &= F(\mathbf{X_1}(t)) \\
~ &+\textcolor{blue}{M_1[\mathbf{X_2}(t-\tau_1)-\mathbf{X_1}(t)]}\\
~ &+\textcolor{red}{M_2[\mathbf{X_2}(t-\tau_2)-\mathbf{X_1}(t)]}
\end{align*}
\begin{align*}
\mathbf{\dot{X_2}}(t) &= F(\mathbf{X_2}(t)) \\
~ &+\textcolor{blue}{M_1[\mathbf{X_1}(t-\tau_1)-\mathbf{X_2}(t)]}\\
~ &+\textcolor{red}{M_2[\mathbf{X_1}(t-\tau_2)-\mathbf{X_2}(t)]}
\end{align*}
\begin{small}
where
\begin{equation*}
F(\mathbf{X_i})=\left\{
\begin{matrix}
x_i(a-x_i)(x_i-1)-y_i \\
bx_i-cy_i
\end{matrix}
\right.
\end{equation*}
\end{small}
\end{minipage}
\begin{minipage}{0.1\textwidth}

\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{Network_Small.eps}
\end{figure}
\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Additional Fixed Points}
Putting 
\begin{align*}
\textcolor{magenta}{\mathbf{X_1}(t-\tau_1)=\mathbf{X_1}(t-\tau_2)=\mathbf{X_1}(t)=\mathbf{X_1^*}}\\
\textcolor{cyan}{\mathbf{X_2}(t-\tau_1)=\mathbf{X_2}(t-\tau_2)=\mathbf{X_2}(t)=\mathbf{X_2^*}}\\
\end{align*}
and
\begin{equation*}
M_1+M_2=M
\end{equation*}
and forcing $\mathbf{\dot{X}_1}(t)=\mathbf{\dot{X}_2}(t)=0$ at the fixed point, we get
\begin{align*}
F(\mathbf{X_1^*})+M[\mathbf{X_1^*}-\mathbf{X_2^*}]=0\\
F(\mathbf{X_2^*})+M[\mathbf{X_2^*}-\mathbf{X_1^*}]=0
\end{align*}
\end{frame}

\begin{frame}{Additional Fixed Points}
\begin{itemize}
\item Characterised by two $x$ and two $y$ co-ordinates
\item Not on the invariant synchronisation manifold
\end{itemize}
\begin{figure}
\includegraphics[width=\textwidth]{Plot_FP_80.eps}
\end{figure}
\end{frame}

\begin{frame}{Additional Fixed Points}
\begin{itemize}
\item Characterised by two $x$ and two $y$ co-ordinates
\item Not on the invariant synchronisation manifold
\end{itemize}
\begin{figure}
\includegraphics[width=\textwidth]{3D_Fixed_Points.eps}
\end{figure}
\end{frame}

\begin{frame}{Single Delay}
\begin{minipage}{0.45\textwidth}
\begin{align*}
\mathbf{\dot{X_1}}(t) &= F(\mathbf{X_1}(t)) \\
~ &+\textcolor{blue}{M_1[\mathbf{X_2}(t-\tau_1)-\mathbf{X_1}(t)]}\\
~ &+\textcolor{red}{M_2[\mathbf{X_2}(t-\tau_2)-\mathbf{X_1}(t)]}
\end{align*}
\begin{align*}
\mathbf{\dot{X_2}}(t) &= F(\mathbf{X_2}(t)) \\
~ &+\textcolor{blue}{M_1[\mathbf{X_1}(t-\tau_1)-\mathbf{X_2}(t)]}\\
~ &+\textcolor{red}{M_2[\mathbf{X_1}(t-\tau_2)-\mathbf{X_2}(t)]}
\end{align*}
\begin{small}
where
\begin{equation*}
F(\mathbf{X_i})=\left\{
\begin{matrix}
x_i(a-x_i)(x_i-1)-y_i \\
bx_i-cy_i
\end{matrix}
\right.
\end{equation*}
\end{small}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{Network_Small.eps}
\end{figure}
\end{minipage}
\end{frame}

\begin{frame}{Single Delay}
\begin{minipage}{0.45\textwidth}
\begin{align*}
\mathbf{\dot{X_1}}(t) &= F(\mathbf{X_1}(t)) \\
~ &+\textcolor{blue}{M_1[\mathbf{X_2}(t-\tau_1)-\mathbf{X_1}(t)]}\\
~ &+\cancel{\textcolor{red}{M_2[\mathbf{X_2}(t-\tau_2)-\mathbf{X_1}(t)]}}
\end{align*}
\begin{align*}
\mathbf{\dot{X_2}}(t) &= F(\mathbf{X_2}(t)) \\
~ &+\textcolor{blue}{M_1[\mathbf{X_1}(t-\tau_1)-\mathbf{X_2}(t)]}\\
~ &+\cancel{\textcolor{red}{M_2[\mathbf{X_1}(t-\tau_2)-\mathbf{X_2}(t)]}}
\end{align*}
\begin{small}
where
\begin{equation*}
F(\mathbf{X_i})=\left\{
\begin{matrix}
x_i(a-x_i)(x_i-1)-y_i \\
bx_i-cy_i
\end{matrix}
\right.
\end{equation*}
\end{small}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{Network_Smal_2l.eps}
\end{figure}
\end{minipage}
\end{frame}

\begin{frame}{Single Delay: Stable Invariant Manifold}
Trajectories close to the manifold converge to it.
\begin{itemize}
\item Oscillators get \textbf{synchronised}
\item Execute \textbf{mixed mode oscillations}
\end{itemize}
\begin{figure}
\includegraphics[width=0.9\textwidth, height=0.55\textheight]{One_Layer_80.eps}
\end{figure}
Only those trajectories which never come close to the invariant manifold are converge to the stable fixed points.
\end{frame}

\begin{frame}{Single Delay: Stable Invariant Manifold}
%Trajectories close to the manifold converge to it.
\begin{itemize}
\item Oscillators get \textbf{synchronised}
\item Execute \textbf{mixed mode oscillations}
\end{itemize}
\begin{figure}
\includegraphics[width=0.9\textwidth]{3D_Final_80.eps}
\end{figure}
Only those trajectories which never come close to the invariant manifold are converge to the stable fixed points.
\end{frame}

\begin{frame}{Single Delay: Stable Invariant Manifold}
%Trajectories close to the manifold converge to it.
\begin{itemize}
\item Oscillators get \textbf{synchronised}
\item Execute \textbf{mixed mode oscillations}
\end{itemize}
\begin{figure}
\includegraphics[width=0.9\textwidth]{Revised_3D_Synchronised.eps}
\end{figure}
Only those trajectories which never come close to the invariant manifold are converge to the stable fixed points.
\end{frame}

\begin{frame}{Single Delay: Unstable Invariant Manifold}
%Trajectories come very close to the invariant manifold before diverging away from it.
\begin{itemize}
\item Oscillators \textbf{loose synchrony} and make \textbf{large excursion away} from the manifold.
\item Either \textbf{converge to the fixed point} or \textbf{come back} close to the invariant manifold.
\end{itemize}
\begin{figure}
\includegraphics[width=0.9\textwidth]{One_Layer_70_2.eps}
\end{figure}
\end{frame}

\begin{frame}{Single Delay: Unstable Invariant Manifold}
Trajectories come very close to the invariant manifold before diverging away from it.
\begin{itemize}
\item Oscillators \textbf{loose synchrony} and make \textbf{large excursion away} from the manifold.
\item Either \textbf{converge to the fixed point} or \textbf{come back} close to the invariant manifold.
\end{itemize}
\begin{figure}
\includegraphics[width=0.9\textwidth]{Revised_3D_Unstable.eps}
\end{figure}
\end{frame}

\begin{frame}{Single Delay: Unstable Invariant Manifold}
Trajectories come very close to the invariant manifold before diverging away from it.
\begin{itemize}
\item Oscillators \textbf{loose synchrony} and make \textbf{large excursion away} from the manifold.
\item Either \textbf{converge to the fixed point} or \textbf{come back} close to the invariant manifold.
\end{itemize}
\begin{figure}
\includegraphics[width=0.9\textwidth]{Revised_3D_Unstable_Manifold.eps}
\end{figure}
\end{frame}

\begin{frame}{Two Types of Events}
\begin{itemize}
\item \textbf{Large Delay:} In-Phase event where trajectory lies on the invariant synchronisation manifold.
\item \textbf{Short Delay:} Out-of-Phase event which occurs away from the invariant synchronisation manifold.
\end{itemize}

\begin{figure}
\begin{subfigure}{0.4\textwidth}
\includegraphics[width=\textwidth, height=0.4\textheight]{InPhaseEvent.eps}
\caption{In-Phase Event}
\end{subfigure}
\begin{subfigure}{0.4\textwidth}
\includegraphics[width=\textwidth, height=0.4\textheight]{OutPhaseEvent.eps}
\caption{Out-of-Phase Event}
\end{subfigure}
\end{figure}
\textbf{But they are not extreme events!!!}
\end{frame}

\begin{frame}{How irregular are the events??}
\textbf{Not irregular enough...}
\begin{figure}
\includegraphics[width=\textwidth]{ISI_One_Layer_Combined.eps}
\caption{Spread in Inter-Event-Interval is not high enough for either {\color{red}short delay} or {\color{blue}long delay} cases for them to be termed extreme event}
\end{figure}
\end{frame}

\begin{frame}{Combining the two cases: Two Delay System}
\begin{minipage}{0.45\textwidth}
\begin{align*}
\mathbf{\dot{X_1}}(t) &= F(\mathbf{X_1}(t)) \\
~ &+\textcolor{blue}{M_1[\mathbf{X_2}(t-\tau_1)-\mathbf{X_1}(t)]}\\
~ &+\textcolor{red}{M_2[\mathbf{X_2}(t-\tau_2)-\mathbf{X_1}(t)]}
\end{align*}
\begin{align*}
\mathbf{\dot{X_2}}(t) &= F(\mathbf{X_2}(t)) \\
~ &+\textcolor{blue}{M_1[\mathbf{X_1}(t-\tau_1)-\mathbf{X_2}(t)]}\\
~ &+\textcolor{red}{M_2[\mathbf{X_1}(t-\tau_2)-\mathbf{X_2}(t)]}
\end{align*}
\begin{small}
where
\begin{equation*}
F(\mathbf{X_i})=\left\{
\begin{matrix}
x_i(a-x_i)(x_i-1)-y_i \\
bx_i-cy_i
\end{matrix}
\right.
\end{equation*}
\end{small}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{Network_Small.eps}
\end{figure}
\end{minipage}
\end{frame}

\begin{frame}{Combining the two cases: Two Delay System}
\begin{itemize}
\item Visibly irregular
\item Comprises of both in-phase and out-of-phase events
\end{itemize}
\begin{figure}
%\includegraphics[width=0.45\textwidth]{OnDiagExEvShort.eps}
%\includegraphics[width=0.45\textwidth]{OffDiagExEvShort.eps}
\includegraphics[width=\textwidth]{Plot_OffDiag_2.eps}
\end{figure}
\end{frame}

\begin{frame}{Combining the two cases: Two Delay System}
\begin{itemize}
\item Visibly irregular
\item Comprises of both in-phase and out-of-phase events
\end{itemize}
\begin{figure}
%\includegraphics[width=0.45\textwidth]{OnDiagExEvShort.eps}
%\includegraphics[width=0.45\textwidth]{OffDiagExEvShort.eps}
\includegraphics[width=\textwidth]{Revised_3D.eps}
\end{figure}
\end{frame}

\begin{frame}{Combining the two cases: Two Delay System}
\begin{itemize}
\item Visibly irregular
\item Comprises of both in-phase and out-of-phase events
\end{itemize}
\begin{figure}
%\includegraphics[width=0.45\textwidth]{OnDiagExEvShort.eps}
%\includegraphics[width=0.45\textwidth]{OffDiagExEvShort.eps}
\includegraphics[width=\textwidth]{Revised_3D_Two_Layer.eps}
\end{figure}
\end{frame}

\begin{frame}{How irregular are the events??}
%\textbf{Not irregular enough...}
\begin{figure}
\includegraphics[width=\textwidth]{ISI_One_Two_Layer.eps}
\caption{Spread in Inter-Event-Interval for FHN units with {\color{magenta}multiple delays} is larger than its single delay counterparts}
\end{figure}
\end{frame}

\begin{frame}{Precursor to Extreme Events}
\begin{small}
\begin{equation*}
\theta_i = \tan ^{-1} \frac{y_i}{x_i}
\end{equation*}
\end{small}
\begin{figure}
\includegraphics[width=0.85\textwidth]{Plot_Phase.eps}
\end{figure}
\textbf{Loss of phase synchrony occurs much prior to the out-of-phase events; allowing it to be used as a precursor to these events.}
\end{frame}

\begin{frame}{In Search of a Mechanism: Chaotic Saddle}
\begin{itemize}
\item Trajectories spend a long time around chaotic saddle
\begin{itemize}
\item In-phase excursions
\item Out-of-Phase excursions
\end{itemize}
\item Finally converge to fixed point
\end{itemize}
\begin{figure}
%\includegraphics[width=0.45\textwidth]{OnDiagExEvShort.eps}
%\includegraphics[width=0.45\textwidth]{OffDiagExEvShort.eps}
\includegraphics[width=\textwidth]{Revised_3D.eps}
\end{figure}
\end{frame}

\begin{frame}{In Search of a Mechanism: Basin of Attraction}
\begin{minipage}{0.49\textwidth}
\begin{figure}
\includegraphics[width=\textwidth, height=\textwidth]{Magnified_Two_M_0_0125.eps}
\end{figure}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\begin{itemize}
\item Tongue-like structures emerging out of the invariant manifold
\item Extremely long transience time to the fixed points
\item Transversally unstable limit-cycles which form the emergence points of the tongues
\end{itemize}
\end{minipage}
%\textbf{Riddling Phenomenon:} Trajectories repeatedly approach an invariant manifold and diverge away from it due to the presence of a transversally unstable invariant set on the manifold.
\end{frame}

%\begin{frame}{In Search of a Mechanism}
%\begin{minipage}{0.49\textwidth}
%\begin{figure}
%\includegraphics[width=\textwidth, height=\textwidth]{LimitCycleBifMagnified.eps}
%\end{figure}
%\end{minipage}
%\begin{minipage}{0.49\textwidth}
%\begin{itemize}
%\item Tongue-like structures emerging out of the invariant manifold
%\item Extremely long transience time to the fixed points
%\item \alert{Transversally unstable limit-cycles which form the emergence points of the tongues}
%\end{itemize}
%\end{minipage}
%\textbf{Riddling Phenomenon:} Trajectories repeatedly approach an invariant manifold and diverge away from it due to the presence of a transversally unstable invariant set on the manifold.
%\end{frame}

\begin{frame}{Conclusions}
\begin{itemize}
\item Extreme events generated in delay-coupled FHN oscillators
\item Dynamics composed of in- and out-of-phase events
\item Distinct channel-like structures identified for each type of event
\item Loss of phase synchrony: precursor of out-of-phase extreme events
\item Mechanism: Some progress made...
\end{itemize}
\begin{block}{References}
\begin{itemize}
\begin{small}
\item A. N. Pisarchik, R. Jaimes-Re\'ategui, R. Sevilla-Escoboza, G. Huerta-Cuellar, and M. Taki, Phys. Rev. Lett. \textbf{107}, 274101 (2011).
\item Rajat Karnatak, Gerrit Ansmann, Ulrike Feudel and Klaus Lehnertz, Phys. Rev. E \textbf{90}, 022917 (2014).
\item A. Rothkegel and K. Lehnertz, New J. Phys. \textbf{16}, 055006 (2014).
\end{small}
\end{itemize}
\end{block}
\begin{large}
\begin{center}
\textbf{Thank You!!!}
\end{center}
\end{large}
\begin{center}
\end{center}

\end{frame}

\end{document}\grid
